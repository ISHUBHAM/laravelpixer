@extends('layouts.app')
@section('content')

    @if (count($errors) > 0)

        @foreach ($errors->all() as $error)

            <p class="alert alert-danger">{{ $error }}</p>

        @endforeach

    @endif

    <div class="container">
        <form action="{{ url('/cregister') }}" method="post" enctype="multipart/form-data">
            @csrf


            <label>
                <h2>Register</h2>
            </label>


            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Name">
            </div>

            <div class="form-group">
                <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
            </div>

            <div class="form-group">
                <input type="number" class="form-control" name="phone" id="exampleInputEmail1" placeholder="Phone">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="username" placeholder="User Name">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" id="exampleInputPassword"
                    placeholder="Password">
            </div>

            <div class="form-group">
                <input type="password" class="form-control" name="password confirmation" placeholder="Confirm Password">
            </div>





            <div class="form-group">
                <input type="text" class="form-control" name="longitude" id="longitude" placeholder="Longitude">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="latitude" id="latitude" placeholder="Lattitude">
            </div>



            <div class="form-group">
                <input type="file" class="form-control" name="image" placeholder="Upload Photo">
            </div>

            <div id="map" style="height: 300px;width:100%;">

            </div><br>

            <div class="form-group">
                <input type="submit" class="form-control btn btn-primary">
            </div>

        </form>
    </div>


@endsection
@section('script')
    <script>
        function initMap() {
            var myLatlng = {
                lat: 28.457523,
                lng: 77.026344
            };

            var map = new google.maps.Map(
                document.getElementById('map'), {
                    zoom: 10,
                    center: myLatlng
                });

            // Create the initial InfoWindow.
            var infoWindow = new google.maps.InfoWindow({
                content: 'Click the map to get Lat/Lng!',
                position: myLatlng
            });
            infoWindow.open(map);

            // Configure the click listener.
            map.addListener('click', function(mapsMouseEvent) {
                // Close the current InfoWindow.
                infoWindow.close();

                // Create a new InfoWindow.
                infoWindow = new google.maps.InfoWindow({
                    position: mapsMouseEvent.latLng
                });
                infoWindow.setContent(mapsMouseEvent.latLng.toString());
                infoWindow.open(map);
                var long = mapsMouseEvent.latLng.toString();
                longlat = long.split(",");
                var long = longlat[0];
                var lat = longlat[1];
                document.getElementById('longitude').value = long;
                document.getElementById('latitude').value = lat;

            });
        }

    </script>
    <script defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeIgLwD1AC7aRZFTUFshXIfNQ32OnCu3M&callback=initMap">
    </script>
@endsection
