@extends('layouts.app')


@section('content')

    @if (session('registered'))

        <p class="alert alert-success">{{ session('registered') }}</p>

    @endif
    @if (session('errmsg'))

        <p class="alert alert-danger">{{ session('errmsg') }}</p>

    @endif

    <div class="container">
        <form action="{{ url('/clogin') }}" method="post"> {{ csrf_field() }}

            <label>
                <h2>Login</h2>
            </label>
            <div class="form-group">
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                    placeholder="Email">
            </div>

            <div class="form-group">
                <input type="password" name="password" class="form-control" id="exampleInputPassword"
                    placeholder="Enter Password">
            </div>


            <div class="form-group">
                <input type="submit" class="form-control btn btn-primary">
            </div>

            <div>
                <div class="form-group row mb-0">
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </div>

        </form>
    </div>



@endsection
