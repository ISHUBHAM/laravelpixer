@extends('layouts.app')

@section('content')




<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">UserName</th>
      <th scope="col">PhoneNo</th>
      <th scope="col">Longitude</th>
      <th scope="col">Lattitude</th>

    </tr>
  </thead>
  <tbody>

  	@foreach ($data as $user)
<tr>
<td>{{ $user->id }}</td>
<td>{{ $user->name}}</td>
<td>{{ $user->email}}</td>
<td>{{ $user->username}}</td>
<td>{{ $user->phone_no}}</td>
<td>{{ $user->longitude}}</td>
<td>{{ $user->lattitude}}</td>

</tr>
@endforeach
    
  </tbody>
</table>







@endsection