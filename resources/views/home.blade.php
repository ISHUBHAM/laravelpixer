@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
{{-- sigh up user details show over here --}}
                   <h6>Name-{{Auth::user()->name }}</h6><br>
                    <h6>EMAIL-{{Auth::user()->email }}</h6><br>
                    <h6>USER NAME-{{Auth::user()->username}}</h6><br>
                    <h6>PHONE NO-{{Auth::user()->phone_no}}</h6><br>
                    <h6>LONGITUDE-{{Auth::user()->longitude }}</h6><br>
                    <h6>LATITUDE-{{Auth::user()->lattitude }}</h6><br>
                    <h6>IMAGE-<img src="{{asset('public/'.Auth::user()->image)}}"  style="height: 300px;width: 300px;"></h6>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
