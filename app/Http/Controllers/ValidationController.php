<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Mail;

class ValidationController extends Controller
{

	public function loginconfirm(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|max:500',
			'password' => 'required',
		], [
			'email.required' => ' please enter your email',
			'password.required' => ' please enter your password',
		]);


		if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
			$user = DB::table('users')
				->select('*')
				->where('email', $request->email)
				->get();
			return view('home', compact('user'));
		} else {
			return redirect(url('/login'))->with('errmsg', 'unable to login');
		}
	}

	public function registerconfirm(Request $request)


	{
        $this->validate($request,[
			'name'=>'required|min:3|max:200',
			'email'=>'required|email|unique:users|max:200',
			'username'=>'required|min:2|max:50',
			// 'phone_no'=>'required|min:10|max:10',
			'password'=>'required|confirmed|max:20',
			'longitude'=>'required|min:2|max:200',
			// 'lattitude'=>'required|min:2|max:200',
			'image'=>'required|max:5000',

		]);




		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->username = $request->username;
		$user->phone_no = $request->phone;
		$user->password = bcrypt($request->password);
		$user->longitude = $request->longitude;
		$user->lattitude = $request->latitude;
		if ($request->hasfile('image')) {
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension();
			$filename = time() . '.' . $extension;
			$file->move('public/', $filename);
			$user->image = $filename;
		}


		$user->save();


		$emaildt["email"] = $request->email;
		$emaildt["name"] = $request->name;
		$emaildt["subject"] = 'Welcome user';



		Mail::send([], $emaildt, function ($message) use ($emaildt) {
			$message->to($emaildt["email"], $emaildt["name"])
				->subject($emaildt["subject"]);
		});

		return redirect(url('/login'))->with('register', 'Register Successful');
	}


	public function getalldata()
	{

       $data= DB::select('call all_show()');
       return view('showall',compact('data'));

	}
}
